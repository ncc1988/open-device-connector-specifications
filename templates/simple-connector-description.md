# ODCS-(I/E)-(Type)-(Type-specific code)

A brief description of the connector.

## Description (optional)

A more detailed description of the connector.

## Pinout

A list of the pins and their function.

1. First pin function
2. Second pin function
3. (and so on)

## Application notes (optional)

Application notes in case some pins have multiple functions.
