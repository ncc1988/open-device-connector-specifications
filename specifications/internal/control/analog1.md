# ODCS-I-C-A1

Analog 1-channel control signal connector.

## Pinout

1. VCC/Vmax
2. Signal
3. GND/Vmin

## Application notes

* Vmax and Vmin define the minimum and maximum voltage of the signal in case the signal is not in the range between VCC and GND.
