# ODCS-I-C-D1R

A digital 1-channel resettable control line connector.

## Description

This connector allows to forward one digital control signal.
The signal source can be controlled by the sink via two
control lines: one for resetting the control signal and another one
for deactivating the control signal source.

## Pinout

1. VCC
2. Control signal
3. Reset
4. Deactivate
5. GND

## Application notes

* The control signal line must be on a low level (GND) by default.
  The control signal is emitted by switching the line to a high level (VCC).
* Sources must implement support for the functions of the reset and the
  deactivate pin.
* Sinks may implement support for the functions of the reset and the
  deactivate pin.
* The reset pin must be on a low level (GND) by default.
  If a reset shall be triggered, it must be pulled to a high level (VCC).
  Sinks that do not use the reset pin must connect it to GND.
* The deactivate pin must be on a low level (GND) by default.
  To trigger deactivation of the control signal source, this pin must be pulled
  to a high level (VCC).
  When this pin is set to a high level, the control signal must not be emitted.
  Sinks that do not use the deactivate pin must connect it to GND.
