# ODCS-I-V-SMCx and ODCS-I-V-SMAx

Internal multiplexed seven segment connector with common cathode (ODCS-I-V-SMC)
or common anode (ODCS-I-V-SMA) and x digits.

## Description

This specification defines a standardised connector for seven segment displays
with a common anode or a common cathode whose digits are multiplexed so that
only one digit is shown at a time.

The "x" in the standard name defines the amount of digits the connector
can handle:

* ODCS-I-V-SMA2: Common anode, two digits
* ODCS-I-V-SMA4: Common anode, four digits
* ODCS-I-V-SMC4: Common cathode, four digits


### Handling of additional display elements

Additional display elements that are not segments A - G and DP are treated as
another digit that has its GND/VCC pin after the GND/VCC pin of the last
full digit.

### Digit order

The digit order is from left to right.

### Segment order

     # A #
    #     #
    F     B
    #     #
     # G #
    #     #
    E     C
    #     #
     # D #   DP

## Pinout

1. A
2. B
3. C
4. D
5. E
6. F
7. G
8. DP
9. Digit 1 GND (common cathode) or VCC (common anode)

Digit 2 and all following digits have their GND or VCC pin after pin 9.
