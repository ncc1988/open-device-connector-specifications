# ODCS-I-V-SACx and ODCS-I-V-SAAx

Internal analog seven segment connector with common cathode (ODCS-I-V-SAC) or
common anode (ODCS-I-V-SAA) and x digits.

## Description

This specification defines a standardised connector for seven segment displays
with a common anode or a common cathode. It is designed for small seven segment
displays that allow all segments to be turned on at the same time without too
much wiring.

The "x" in the standard name defines the amount of digits the connector
can handle:

* ODCS-I-V-SAA1: Common anode, one digit
* ODCS-I-V-SAA2: Common anode, two digits
* ODCS-I-V-SAC2: Common cathode, two digits


### Handling of additional display elements

Additional display elements that are not segments A - G and DP are treated as
another digit whose pins are placed after the last full digit.

### Digit order

The digit order is from left to right.

### Segment order

     # A #
    #     #
    F     B
    #     #
     # G #
    #     #
    E     C
    #     #
     # D #   DP

## Pinout

1. GND (common cathode) / VCC (common anode)
2. Digit 1 A
3. Digit 1 B
4. Digit 1 C
5. Digit 1 D
6. Digit 1 E
7. Digit 1 F
8. Digit 1 G
9. Digit 1 DP

Digit 2 and all following digits have the same pin order as Digit 1 pins 2 to 9.
