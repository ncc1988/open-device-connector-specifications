# ODCS-I-A-SP

Internal stereo audio speaker connector.

## Pinout

1. Left channel +
2. Left channel -
3. Right channel +
4. Right channel -
