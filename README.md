# open-device-connector-specifications

This repository contains electronic device connector specifications that can be used to build modular electronic devices.

## The problem

Circuit boards often have all modules of a device on the same board which makes it hard to replace one module with a better one. Devices that use several boards have the problem of proprietary connectors between the boards where the pin assignment is not standardised.

## The solution

Bringing the unix philosophy to electronic devices: Standardised connectors allow building modular devices where each module can be designed to do one job and to do that job well.

## Specification overview

This specification is meant for internal and external device connectors, although at the moment, all specified connectors are internal connectors.

### Common definitions

#### Source

A source is that end of a connection that produces data, power or signals.

#### Sink

A sink is that end of a connection that consumes data, power or signals.

#### Sink connector

A sink connector is the connector for a sink. It must be made out of connection pins.

#### Source connector

A source connector is the connector for a source. It must be made out of connection jacks.

#### Pin raster

The standard pin raster is 2.54mm. For connectors with less than 5 pins, a raster of 2.5mm is acceptable.


### Naming scheme

Each connector specification follows this naming scheme:

ODCS-(area)-(type)-(type-specific suffix)

* area: I for internal, E for external connectors
* type: The type code of the connector:
  * A: Audio
  * C: Control signals
  * D: Data
  * P: Power
  * V: Visual signals (video, light)
  * (more to come)

Examples:

* ODCS-I-A-S: ODCS internal stereo audio connector
* ODCS-I-C-D2@50: ODCS internal two-bit digital 5V level control signal connector 
* ODCS-I-D-8@33: ODCS internal 3.3V level 8-bit data connector

### Directory structure

* The specifications for the device connectors are placed in the "specifications" folder. 
* Templates for device connector specifications are placed in the "templates" folder.
* A KiCad library with parts for the specified connectors is in the "lib" folder.
